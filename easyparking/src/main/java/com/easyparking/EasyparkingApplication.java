package com.easyparking;

import com.easyparking.modelos.Clientes;
import com.easyparking.modelos.Estacionamientos;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam; // MVC - Sirve para acceder al valor de uno de los parametros. Se usa en las consultas (GET)
import org.springframework.web.bind.annotation.RestController; // SpringBoot - Recibe peticiones HTTP y las responde
import com.google.gson.Gson; // JSON - Cuando hacemos una petición a un servicio el nos retorna una json
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@SpringBootApplication
@RestController
public class EasyparkingApplication {

    @Autowired(required = true) // Sirve para interpretar una clase sin necesidad de decirle que cree una instancia nueva. Equivalente Cuenta c = new Cuenta();
    Estacionamientos e; // Instancias - Objeto  Cuenta c = new Cuenta();
    @Autowired(required = true)
    Clientes c;

    public static void main(String[] args) {
        SpringApplication.run(EasyparkingApplication.class, args);
    }

    @GetMapping("/clientdata") // es una anotación de Spring Boot que nos permite simplicar el manejo de los diferentes métodos de Spring M
    public String consultarCliente(@RequestParam(value = "docu", defaultValue = "1") int docu) throws ClassNotFoundException, SQLException {
        c.setDocumento(docu); // Le pasamos eldocumento al método setId y luego de esto validamos si existe o no
        // con el if se trae el primer registro que encuentre
        if (c.consultar()) { // True
            String res = new Gson().toJson(c); // Creamos un json vacio
            c.setId_cliente(0); // Si hay más registros SOLO traigase el primero que encuentre
            c.setNombre("");
            c.setDocumento(0);
            c.setTelefono(0);
            c.setPlaca_vehiculo("");
            return res; // retorna un JSON para la request o petición al servidor.
            // else devolver el json vacio
        } else { // False           
            return new Gson().toJson(c);
        }
    }

    @GetMapping("/clientexiste")
    public String existeCliente(@RequestParam(value = "idclien", defaultValue = "1") int idclien) throws ClassNotFoundException, SQLException {
        e.setFk_id_cliente(idclien);
        if (e.consultar()) {
            return new Gson().toJson(e);
        } else {
            return new Gson().toJson(e);
        }
    }

    // GET - POST - PUT - DELETE
    // POST: Enviar un dato a al servidor, también podemos efectuar cambios enviando la petición post al servidor(Actualización)
    // PUT: Actualizar (Reemplazar)
    @PostMapping(path = "/clientup", // le enviamos datos al servidor
            consumes = MediaType.APPLICATION_JSON_VALUE, // recibe json Nombre= Carlos
            produces = MediaType.APPLICATION_JSON_VALUE) // genera un json  => {nombre = Carlos}
    // definición
    // RequestBody: Recibe los datos que enviaremos en el cuerpo de la petición utilizando HTTP POST
    // Le enviamos datos al servidor y el los resgitra en la BD

    // RequestBody sirve para hacer actualizaciones y guardar datos nuevos en la BD 
    public String actualizarCuenta(@RequestBody String docu) throws ClassNotFoundException, SQLException {
        Clientes f = new Gson().fromJson(docu, Clientes.class); // recibimos el json y lo devolvemos un objeto estudiante
        c.setId_cliente(f.getId_cliente()); // Si hay más registros SOLO traigase el primero que encuentre
        c.setNombre(f.getNombre());
        c.setDocumento(f.getDocumento());
        c.setTelefono(f.getTelefono());
        c.setPlaca_vehiculo(f.getPlaca_vehiculo());
        c.actualizar(); // ejecuta el método actualizar con los datos que se le enviaron del json y actualiza la BD
        return new Gson().toJson(c); // cliente le envia json al servidor. fpr de comunicarse
    }


}
