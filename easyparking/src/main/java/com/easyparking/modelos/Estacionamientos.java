package com.easyparking.modelos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

@Component("estacionamientos")
public class Estacionamientos {

    @Autowired
    transient JdbcTemplate jdbcTemplate;

    //Atributos
    private int id_estacionamiento;
    private String codigo_estacion;
    private String fechahora_ingreso;
    private String fechahora_salida;
    private int costo;
    private Clientes fk_id_cliente;

    //Constructor
    public Estacionamientos(int id_estacionamiento, String codigo_estacion, String fechahora_ingreso, String fechahora_salida, int costo) {
        super();
        this.id_estacionamiento = id_estacionamiento;
        this.codigo_estacion = codigo_estacion;
        this.fechahora_ingreso = fechahora_ingreso;
        this.fechahora_salida = fechahora_salida;
        this.costo = costo;
    }

    public Estacionamientos() {
    }

    //Metodos
    public int getId_estacionamiento() {
        return id_estacionamiento;
    }

    public void setId_estacionamiento(int id_estacionamiento) {
        this.id_estacionamiento = id_estacionamiento;
    }

    public String getCodigo_estacion() {
        return codigo_estacion;
    }

    public void setCodigo_estacion(String codigo_estacion) {
        this.codigo_estacion = codigo_estacion;
    }

    public String getFechahora_ingreso() {
        return fechahora_ingreso;
    }

    public void setFechahora_ingreso(String fechahora_ingreso) {
        this.fechahora_ingreso = fechahora_ingreso;
    }

    public String getFechahora_salida() {
        return fechahora_salida;
    }

    public void setFechahora_salida(String fechahora_salida) {
        this.fechahora_salida = fechahora_salida;
    }

    public int getCosto() {
        return costo;
    }

    public void setCosto(int costo) {
        this.costo = costo;
    }

    public int getFk_id_cliente() {
        return fk_id_cliente.getId_cliente();
    }

    public void setFk_id_cliente(int fk_id_cliente) {
        this.fk_id_cliente.setId_cliente(fk_id_cliente);
    }

    @Override
    public String toString() {
        return "Estacionamientos{" + "id_estacionamiento=" + id_estacionamiento + ", codigo_estacion=" + codigo_estacion + ", fechahora_ingreso=" + fechahora_ingreso + ", fechahora_salida=" + fechahora_salida + ", costo=" + costo + ", fk_id_cliente=" + fk_id_cliente + '}';
    }

    //CRUD - C
    public String guardar() throws ClassNotFoundException, SQLException {
        String sql = "INSERT INTO estacionamientos(id_estacionamiento, codigo_estacion, fechahora_ingreso, fechahora_salida, costo) VALUES(?,?,?,?,?)";
        return sql;
    }

    //CRUD - R
    public boolean consultar() throws ClassNotFoundException, SQLException {
        String sql = "SELECT id_estacionamiento, codigo_estacion, fechahora_ingreso, fechahora_salida, costo FROM estacionamientos WHERE fk_id_cliente=?";
        List<Estacionamientos> estacionamientos = jdbcTemplate.query(sql, (rs, rowNum)
                -> new Estacionamientos(
                        rs.getInt("id_estacionamiento"),
                        rs.getString("codigo_estacion"),
                        rs.getString("fechahora_ingreso"),
                        rs.getString("fechahora_salida"),
                        rs.getInt("costo")
                ), new Object[]{this.getFk_id_cliente()});
        if (estacionamientos != null && !estacionamientos.isEmpty()) {
            this.setId_estacionamiento(estacionamientos.get(0).getId_estacionamiento());
            this.setCodigo_estacion(estacionamientos.get(0).getCodigo_estacion());
            this.setFechahora_ingreso(estacionamientos.get(0).getFechahora_ingreso());
            this.setFechahora_salida(estacionamientos.get(0).getFechahora_salida());
            this.setCosto(estacionamientos.get(0).getCosto());

            return true;
        } else {
            return false;
        }
    }

    public List<Estacionamientos> consultarTodo(int fk_id_cliente) throws ClassNotFoundException, SQLException {
        String sql = "SELECT id_estacionamiento, codigo_estacion, fechahora_ingreso, fechahora_salida, costo FROM estacionamientos WHERE fk_id_cliente=?";
        List<Estacionamientos> estacionamientos = jdbcTemplate.query(sql, (rs, rowNum)
                -> new Estacionamientos(
                        rs.getInt("id_estacionamiento"),
                        rs.getString("codigo_estacion"),
                        rs.getString("fechahora_ingreso"),
                        rs.getString("fechahora_salida"),
                        rs.getInt("costo")
                ), new Object[]{this.getFk_id_cliente()});

        return estacionamientos;
    }

    // CRUD - U
    public String actualizar() throws ClassNotFoundException, SQLException {
        String sql = "UPDATE estacionamientos SET id_estacionamiento =?, codigo_estacion=?, fechahora_ingreso=?, fechahora_salida=?, costo=? WHERE id_estacionamiento =?";

        return sql;
    }

    //CRUD - D
    public boolean borrar() throws SQLException, ClassNotFoundException {

        String sql = "DELETE FROM cuentas WHERE id_estacionamiento = ?";
        Connection c = jdbcTemplate.getDataSource().getConnection();
        PreparedStatement ps = c.prepareStatement(sql);
        ps.setInt(1, this.getId_estacionamiento());
        ps.execute();
        ps.close();

        return true;
    }

}
