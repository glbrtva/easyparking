package com.easyparking.modelos;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

@Component("clientes")
public class Clientes {

    @Autowired
    transient JdbcTemplate jdbcTemplate;

//Atributos
    private int id_cliente;
    private String nombre;
    private int documento;
    private int telefono;
    private String placa_vehiculo;

    //Constructor
    public Clientes(int id_cliente, String nombre, int documento, int telefono, String placa_vehiculo) {
        this.id_cliente = id_cliente;
        this.nombre = nombre;
        this.documento = documento;
        this.telefono = telefono;
        this.placa_vehiculo = placa_vehiculo;
    }

    public Clientes() {
    }

    //Metodos
    public int getId_cliente() {
        return id_cliente;
    }

    public void setId_cliente(int id_cliente) {
        this.id_cliente = id_cliente;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getDocumento() {
        return documento;
    }

    public void setDocumento(int documento) {
        this.documento = documento;
    }

    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    public String getPlaca_vehiculo() {
        return placa_vehiculo;
    }

    public void setPlaca_vehiculo(String placa_vehiculo) {
        this.placa_vehiculo = placa_vehiculo;
    }

    @Override
    public String toString() {
        return "Clientes{" + "id_cliente=" + id_cliente + ", nombre=" + nombre + ", documento=" + documento + ", telefono=" + telefono + ", placa_vehiculo=" + placa_vehiculo + '}';
    }

    //CRUD - R
    public boolean consultar() throws ClassNotFoundException, SQLException {
        String sql = "SELECT id_cliente, nombre, documento, telefono, placa_vehiculo WHERE placa_vehiculo = ?";
        List<Clientes> clientes = jdbcTemplate.query(sql, (rs, rowNum)
                -> new Clientes(
                        rs.getInt("id_cliente"),
                        rs.getString("nombre"),
                        rs.getInt("documento"),
                        rs.getInt("telefono"),
                        rs.getString("placa_vehiculo")
                ), new Object[]{this.getId_cliente()});
        if (clientes != null && !clientes.isEmpty()) {
            this.setId_cliente(clientes.get(0).getId_cliente());
            this.setNombre(clientes.get(0).getNombre());
            this.setDocumento(clientes.get(0).getDocumento());
            this.setTelefono(clientes.get(0).getTelefono());
            this.setPlaca_vehiculo(clientes.get(0).getPlaca_vehiculo());

            return true;
        } else {
            return false;
        }
    }

    //CRUD - U
    public void actualizar() throws ClassNotFoundException, SQLException {
        String sql = "Update clientes SET nombre = ?, documento = ?, telefono = ?, placa_vehiculo = ? WHERE documento = ?";
        Connection c = jdbcTemplate.getDataSource().getConnection();
        PreparedStatement ps = c.prepareStatement(sql);
        ps.setString(2, this.getNombre());
        ps.setInt(3, this.getDocumento());
        ps.setInt(4, this.getTelefono());
        ps.setString(5, this.getPlaca_vehiculo());
        ps.executeUpdate();
        ps.close();
    }

}
