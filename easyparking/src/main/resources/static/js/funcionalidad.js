/* global angular */

// JavaScript source code
angular.module('parqueo',[])
.controller('filasCuentas',function($scope,$http){
    
    $scope.nombre = "";
    $scope.documento = 0;
    $scope.telefono = 0;
    $scope.nplaca = "";
    
    // accion del boton consultar
    $scope.consultar = function(){
        if($scope.id === undefined || $scope.id === null){
            $scope.id = 0;
        }
        
        $http.get("/easyparking/clientdata?docu="+$scope.docu).then(function(data){
            console.log(data.data);
            $scope.nombre = data.data.nombre;
            $scope.documento = data.data.documento;
            $scope.telefono = data.data.telefono;
            $scope.nplaca = data.data.nplaca;
        },function(){
             //error
            $scope.nombre = "";
            $scope.documento = 0;
            $scope.telefono = 0;
            $scope.nplaca = "";
            $scope.filas = []; // guarda
        });
        
        $http.get("/easyparking/clientexiste?idclien="+$scope.idclien).then(function(data){
            console.log(data.data);
            $scope.filas = data.data;  
        },function(){
            $scope.filas = [];
        });
    };
    /*
    //acción del botón actualizar
    $scope.actualizar = function(){
        if($scope.cedula === undefined || $scope.cedula === null){
            $scope.cedula = 0;
        }
        data = {
            "id": $scope.cedula,
            "nombre":$scope.nombre,
            "edad": $scope.edad,
            "correo": $scope.correo
        }
        $http.post('/easyparking/clienup', data).then(function(data){
            //success
            $scope.nombre = data.data.nombre;
            $scope.edad = data.data.edad;
            $scope.correo = data.data.correo;
        },function(){
            //error
            $scope.nombre = "";
            $scope.edad = "";
            $scope.correo = "";
            $scope.filas = [];
        });
        
    };
    
    //acción del botón actualizar
    $scope.guardar = function(){
        data = {
            "nombre":$scope.nombre,
            "edad": $scope.edad,
            "correo": $scope.correo
        };
        $http.post('/cajero/estudiante', data).then(function(data){
            //success
            $scope.nombre = data.data.nombre;
            $scope.edad = data.data.edad;
            $scope.correo = data.data.correo;
        },function(){
            //error
            $scope.nombre = "";
            $scope.edad = "";
            $scope.correo = "";
            $scope.filas = [];
        });
        
    };
    
    
    // acción del botón borrar aun no creada en application.java
    $scope.borrar = function(){
        if($scope.idcuenta === undefined || $scope.idcuenta === null){
            $scope.idcuenta = 0;
        }
        data = {
            "id": $scope.idcuenta
        }
        $http.delete('/cajero/eliminarcuenta/'+$scope.idcuenta, data).then(function(data){
            alert("La cuenta ha sido eliminada");
            
        },function(){
            alert("Ha ocurrido un error");
        });
    };  */
});



